function love.conf(t)
	t.title = "CASHEW"
    t.version = "11.1"
    t.console = true
    t.identity = "CASHEW"
	t.gammacorrect = true
	t.window.icon = nil
end
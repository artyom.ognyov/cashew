local saving = {}

function saving.all(map,path)
	
	file = io.open(path,"w")

	for i = 1, #map do
		for j = 1, #map[i] do
			file:write(map[i][j])
		end
		if i~= #map then
			file:write("\n")
		end
	end

	file:close()

end

function saving.string(map,path)
	
	file = io.open(path,"w")

	for i = 1, #map do
		file:write(map[i])
		if i~= #map then
			file:write("\n")
		end
	end

	file:close()

end

return saving
-- THIS IS FILE WITH ALL COLOR SCHEMES FOR CHAPTERS

function sixteenToNumber(char)
	if char == '0' then
		return 0
	elseif char == '1' then
		return 1
	elseif char == '2' then
		return 2
	elseif char == '3' then
		return 3
	elseif char == '4' then
		return 4
	elseif char == '5' then
		return 5
	elseif char == '6' then
		return 6
	elseif char == '7' then
		return 7
	elseif char == '8' then
		return 8
	elseif char == '9' then
		return 9
	elseif (char == 'A') or (char == 'a') then
		return 10
	elseif (char == 'B') or (char == 'b') then
		return 11
	elseif (char == 'C') or (char == 'c') then
		return 12
	elseif (char == 'D') or (char == 'd') then
		return 13
	elseif (char == 'E') or (char == 'e') then
		return 14
	elseif (char == 'F') or (char == 'f') then
		return 15
	else
		return 0
	end
end

function hexToRgb(hexText)
	local rgbArray = {0, 0, 0}
	rgbArray[1] = (sixteenToNumber(string.sub(hexText, 1, 1)) * 16 + sixteenToNumber(string.sub(hexText, 2, 2))) / 255
	rgbArray[2] = (sixteenToNumber(string.sub(hexText, 3, 3)) * 16 + sixteenToNumber(string.sub(hexText, 4, 4))) / 255
	rgbArray[3] = (sixteenToNumber(string.sub(hexText, 5, 5)) * 16 + sixteenToNumber(string.sub(hexText, 6, 6))) / 255
	return rgbArray
end

function setColorScheme(name)
	if name == "default" then
		--ink
		realWhite = hexToRgb('E9EFD7')
		light = hexToRgb('95A1B2')
		middle = hexToRgb('585F6F')
		dark = hexToRgb('403941')
		realBlack = hexToRgb('000000')
	elseif name == "chapter1" then
		--nes
		realWhite = hexToRgb('F6E6C5')
		light = hexToRgb('D58D48')
		middle = hexToRgb('A53522')
		dark = hexToRgb('321B4F')
		realBlack = hexToRgb('000000')
	elseif name == "chapter2" then
		--traveler
		realWhite = hexToRgb('FDC13F')
		light = hexToRgb('97C3D0')
		middle = hexToRgb('DD4A42')
		dark = hexToRgb('2D355A')
		realBlack = hexToRgb('000000')
	elseif name == "chapter3" then
		--sunset
		realWhite = hexToRgb('FCEFC3')
		light = hexToRgb('F6A891')
		middle = hexToRgb('819FC1')
		dark = hexToRgb('4B344C')
		realBlack = hexToRgb('000000')
	elseif name == "chapter4" then

	elseif name == "chapter5" then

	elseif name == "chapter6" then

	else
		--ink
		realWhite = hexToRgb('E9EFD7')
		light = hexToRgb('95A1B2')
		middle = hexToRgb('585F6F')
		dark = hexToRgb('403941')
		realBlack = hexToRgb('000000')
	end

	white = realWhite
	black = realBlack
	love.graphics.setBackgroundColor(black)

end

function setChapterColorScheme()
	if currentChapter == 1 then
		setColorScheme("chapter1")
	elseif currentChapter == 2 then
		setColorScheme("chapter2")
	elseif currentChapter == 3 then
		setColorScheme("chapter3")
	elseif currentChapter == 4 then
		setColorScheme("chapter4")
	elseif currentChapter == 5 then
		setColorScheme("chapter5")
	elseif currentChapter == 6 then
		setColorScheme("chapter6")
	end
end
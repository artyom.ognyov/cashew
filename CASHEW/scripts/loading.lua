local loading = {}

function loading.all(path)

	file = io.open(path,"r")
	io.input(file)
	s = {}
	m = {}

	i = 1
	while true do
			s[i] = io.read() or nil
			if s[i] == nil then break end
			i = i+1
	end

	i = i-1
	for j = 1, i do
		m[j] = {}
	end

	for j = 1, i do
		for l = 1, #s[j] do
			m[j][l] = string.sub(s[j], l, l)
		end
	end

	file:close()

	return m

end

function loading.string(path)

	file = io.open(path,"r")
	io.input(file)
	s = {}

	i = 1
	while true do
			s[i] = io.read() or nil
			if s[i] == nil then break end
			i = i+1
	end

	file:close()

	return s

end

return loading
-- FUNCTIONS FOR ANIMATIONS

-- animations with scaling Y

function animationsUI()

	if menuswitch == 1 then
		if menuscale > 0 then
			menuscale = menuscale - 0.05
		else
			menuswitch = 2
			setChapterColorScheme()
			mapN = currentLevel
			menuscale = 0
			gamescale = 0
		end
	end

	if menuswitch == 2 then
		if gamescale < 1 then
			gamescale = gamescale + 0.05
		else
			menuswitch = 0
			gamescale = 1
		end
	end



	if menuswitch == -1 then
		if gamescale > 0 then
			gamescale = gamescale - 0.05
		else
			menuswitch = -2
			gamescale = 0
			setColorScheme("default")
			mapN = 0
		end
	end

	if menuswitch == -2 then
		if menuscale < 1 then
			menuscale = menuscale + 0.05
		else
			menuswitch = 0
			menuscale = 1
		end
	end


	if quitSwitch == 1 then
		if quitScale > 0 then
			quitScale = quitScale - 0.05
		else
			quitScale = 0
			--saveAll()
			love.event.quit()
		end
	end



	if settingsSwitch == 1 then -- to settings IN
		if settingsScale > 0 then
			settingsScale = settingsScale - 0.05
		else
			settingsScale = 0
			settingsSwitch = 2
			settingsMenu.i = 1
			mapN = -3
		end
	elseif settingsSwitch == 2 then -- to settings OUT
		if settingsScale < 1 then
			settingsScale = settingsScale + 0.05
		else
			settingsScale = 1
			settingsSwitch = 0
		end
	elseif settingsSwitch == -1 then -- from settings OUT
		if settingsScale > 0 then
			settingsScale = settingsScale - 0.05
		else
			settingsScale = 0
			settingsSwitch = -2
			mapN = 0
		end
	elseif settingsSwitch == -2 then -- from settings OUT
		if settingsScale < 1 then
			settingsScale = settingsScale + 0.05
		else
			settingsScale = 1
			settingsSwitch = 0
		end
	end



	if aboutSwitch == 1 then -- to about IN
		if aboutScale > 0 then
			aboutScale = aboutScale - 0.05
		else
			aboutScale = 0
			aboutSwitch = 2
			mapN = -1
		end
	elseif aboutSwitch == 2 then -- to about OUT
		if aboutScale < 1 then
			aboutScale = aboutScale + 0.05
		else
			aboutScale = 1
			aboutSwitch = 0
		end
	elseif aboutSwitch == -1 then -- from about OUT
		if aboutScale > 0 then
			aboutScale = aboutScale - 0.05
		else
			aboutScale = 0
			aboutSwitch = -2
			mapN = 0
		end
	elseif aboutSwitch == -2 then -- from about OUT
		if aboutScale < 1 then
			aboutScale = aboutScale + 0.05
		else
			aboutScale = 1
			aboutSwitch = 0
		end
	end

	if chapterMenuSwitch == 1 then -- to chapterMenu IN
		if chapterMenuScale > 0 then
			chapterMenuScale = chapterMenuScale - 0.05
		else
			chapterMenuScale = 0
			chapterMenuSwitch = 2
			mapN = -2
		end
	elseif chapterMenuSwitch == 2 then -- to chapterMenu OUT
		if chapterMenuScale < 1 then
			chapterMenuScale = chapterMenuScale + 0.05
		else
			chapterMenuScale = 1
			chapterMenuSwitch = 0
		end
	elseif chapterMenuSwitch == -1 then -- from chapterMenu IN
		if chapterMenuScale > 0 then
			chapterMenuScale = chapterMenuScale - 0.05
		else
			chapterMenuScale = 0
			chapterMenuSwitch = -2
			mapN = 0
		end
	elseif chapterMenuSwitch == -2 then -- from chapterMenu OUT
		if chapterMenuScale < 1 then
			chapterMenuScale = chapterMenuScale + 0.05
		else
			chapterMenuScale = 1
			chapterMenuSwitch = 0
		end
	elseif chapterMenuSwitch == 3 then -- from chapterMenu to Game IN
		if chapterMenuScale > 0 then
			chapterMenuScale = chapterMenuScale - 0.05
		else
			chapterMenuSwitch = 0
			chapterMenuScale = 0
			mapN = currentChapter
			setChapterColorScheme()
			menuscale = 0
			menuswitch = 0
			gamescale = 1
		end
	end
	
	
	
	if dialogswitch == 1 then
		if dialogscale < 1 then
			dialogscale = dialogscale + 0.15
		else
			dialogscale = 1
			dialogswitch = 0
		end
	end
	
	if dialogswitch == -1 then
		if dialogscale > 0 then
			dialogscale = dialogscale - 0.15
		else
			dialogscale = 0
			dialogswitch = 0
			istalking = false
		end
	end
		

	if inventoryswitch == 1 then
		if inventoryscale < 1 then
			inventoryscale = inventoryscale + 0.1
			if inventoryscale > 1 then
				inventoryscale = 1
			end
		else
			inventoryscale = 1
			inventoryswitch = 0

			if currentChapter == 1 and defaults.chapter1.inventory1 == false then
				defaults.chapter1.inventory1 = true
			end

			inventory.state = true
			inventory.current = {}
			inventory.show = {}
			inventory.current.number = 0
			local i
			local j = 1

			if currentChapter == 1 then
				for i = 1, #inventory.all.chapter1 do
					if inventory.all.chapter1[i][3] == true then
						inventory.current[j] = inventory.all.chapter1[i]
						j = j+1
					end
				end
			end
			inventory.current.number = #inventory.current
			if inventory.current.number > 0 then
				inventory.current.position = 1
				inventory.show.position = 1
			end

		end
	end

	if inventoryswitch == -1 then
		if inventoryscale > 0 then
			inventoryscale = inventoryscale - 0.1
			if inventoryscale < 0 then
				inventoryscale = 0
			end
		else
			inventoryscale = 0
			inventoryswitch = 0
			inventory.state = false
		end
	end

	-- end of animations
end
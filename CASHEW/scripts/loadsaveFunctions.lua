-- FUNCTIONS FOR LOAD AND SAVE

function file_exists(path)
	local f = io.open(path,"r")
	if f ~= nil then io.close(f)
		return true
	else
		return false
	end
end

function checkMapSize(map)
	
	mapSize = 0

	for d = 1, #map do
		if mapSize < #map[d] then
			mapSize = #map[d]
		end
	end

	if mapSize > #map then
		mapSize = #map
	end

	return mapSize
end

function saveAll()
	
	map[player.y][player.x] = player.floor
	-- CHAPTER 1
	saving.all(chapter1.map1,"chapter1map1.save")
	saving.all(chapter1.map2,"chapter1map2.save")
	

	local numbers = {}
	numbers[1] = player.x
	numbers[2] = player.y
	numbers[3] = player.floor
	numbers[4] = progress
	numbers[5] = player.coins
	saving.string(numbers,"numbers.save")

end
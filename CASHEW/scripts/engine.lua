--[[
	THIS IS THE MAIN FUNCTION. READ ME!
]]--

require ("utf8")
saving = require "scripts/saving"
loading = require "scripts/loading"
Timer = require "scripts/timer"

--███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗    ███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
--██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║    ██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
--███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║    █████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
--╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║    ██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
--███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║    ██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
--╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝    ╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝

-- requiring system functions
require ("scripts/mapFunctions")
require ("scripts/mapObjectsFunctions")
require ("scripts/dialogFunctions")
require ("scripts/enemiesFunctions")
require ("scripts/loadsaveFunctions")
require ("scripts/audioFunctions")
require ("scripts/animationFunctions")
require ("scripts/overloadGraphicsFunctions")
require ("scripts/colors")

--██╗      ██████╗  █████╗ ██████╗ 
--██║     ██╔═══██╗██╔══██╗██╔══██╗
--██║     ██║   ██║███████║██║  ██║
--██║     ██║   ██║██╔══██║██║  ██║
--███████╗╚██████╔╝██║  ██║██████╔╝
--╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 

function engineLoad()

	-- get os name
	OS = love.system.getOS()

	-- MAPS
	if file_exists("numbers.save") then
		chapter1 = {}
		chapter1.map1 = loading.all("chapter1map1.save")
		chapter1.map2 = loading.all("chapter1map2.save")
	else
		chapter1 = {}
		chapter1.map1 = loading.all("chapter1map001.default")
		chapter1.map2 = loading.all("chapter1map002.default")
		chapter1.map3 = loading.all("chapter1map003.default")
		chapter1.map4 = loading.all("chapter1map004.default")
		chapter1.map5 = loading.all("chapter1map005.default")
		chapter1.map6 = loading.all("chapter1map006.default")
		chapter1.map7 = loading.all("chapter1map007.default")
		chapter1.map8 = loading.all("chapter1map008.default")
		chapter1.map9 = loading.all("chapter1map009.default")
		chapter1.map10 = loading.all("chapter1map010.default")
	end

	love.graphics.setDefaultFilter('nearest','nearest')

--dialogs

	pass = 1

	keyIsReadyToBePressed = true

	n = 0
	tipIsComplete = false
	istalking = false
	talkingLetters = 0
	exitDialog = false
	goEnter = false
	freeze = false
	mute = fastMute
	globalVolume = 100
	hudGlobalVolume = globalVolume
	gamescale = 1
	menuscale = 1
	menuswitch = 0
	dialogscale = 0
	dialogswitch = 0
	languageAtStartScale = 1
	languageAtStartSwitch = -1
	settingsSwitch = 0
	aboutSwitch = 0
	chapterMenuSwitch = 0
	chapterIntro = false
	canBeContinued = false
	quitSwitch = 0
	
	global05 = true
	global1 = true
	global2 = true

	-- System vars
	
	player = {}

	if file_exists("numbers.save") then
		local numbers = {}
		numbers = loading.string("numbers.save")
		player.x = tonumber(numbers[1])
		player.y = tonumber(numbers[2])
		player.floor = numbers[3]
		progress = tonumber(numbers[4])
		player.coins = tonumber(numbers[5])
	else
		--remove logic TODO
		if fastLevel == 0 then
			player.x = 11
			player.y = 27
		else
			player.x = fastX
			player.y = fastY
		end
		player.floor = " "
		progress = 1
		player.coins = 0
		--player.weaponDistance = 5 --temp while no inventory weapon
	end

	mapOut = {
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
		{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '}
	}
	map = {}
	
	-- remove logic
	currentChapter = 1
	if fastLevel == 0 then
		currentLevel = 1
		mapN = -5
		beginning = 0
		opacity = 0
		timer = 0
	else
		currentLevel = fastLevel --change to 1
		mapN = currentLevel
		beginning = -1
		opacity = 1
		timer = 1
	end

	map = setCurrentMap(currentLevel)

	mapSize = checkMapSize(map)
	map[player.y][player.x] = '@'

	defaults = {}
	defaults.chapter1 = {}
	defaults.chapter1.talkedWithUsher = false
	defaults.chapter1.inventory1 = false

	solidAre = "abcdefgjklmnopqrstuvwxyz.~^#-|+?;[]"
	movableAre = "#"
	enemiesAre = "bcdegjklmnopqrstuvwyz"
	darkAre = "ABCDEFGHIJCLMNOPQRSTUVWXYZ.~=[]<>()"

	hud = {}
	hud.m1 = " "
	hud.m2 = " "
	hud.m3 = " "
	hud.arrow = false
	lighter = 0
	hud.jump = 0
	hud.jumpState = 1
	hud.jumpSpeed = 0.1
	hud.allblack = 0
	hud.allbackState = 1
	hud.allblackSpeed = 0.1
	hud.prevPop = " "
	hud.popSwitch = 0
	hud.popScale = 0

	inventory = {}
	inventory.state = false
	inventory.show = {}
	inventory.show.position = 1
	inventory.current = {}
	inventory.current.number = 0
	inventory.current.position = 1

		enemiesTick = {
		a = true,
		b = true,
		c = true,
		d = true,
		e = true,
		f = true,
		g = true,
		j = true,
		k = true,
		l = true,
		m = true,
		n = true,
		o = true,
		p = true,
		q = true,
		r = true,
		s = true,
		t = true,
		u = true,
		v = true,
		w = true,
		y = true,
		z = true
	}

	love.keyboard.setKeyRepeat(true)
	ismoving = true
	globaltick = true

	menu = {}
	menu.en = {"CONTINUE", "NEW GAME", "SETTINGS", "ABOUT", "QUIT"}
	menu.ru = {"ПРОДОЛЖИТЬ", "НОВАЯ ИГРА", "НАСТРОЙКИ", "ОБ ИГРЕ", "ВЫХОД"}
	menu.i = 1

	language = "ru"
	languageAtStart = 2

	settingsMenu = {}
	settingsMenu.en = {"VOLUME", "FULLSCREEN", "LANGUAGE", "RETRO MODE"}
	settingsMenu.ru = {"ГРОМКОСТЬ", "ПОЛНЫЙ ЭКРАН", "ЯЗЫК", "РЕТРО РЕЖИМ"}
	settingsMenu.i = 1

	chapterMenu = {}
	chapterMenu.en = {
		"CARTRIDGE 1: NUSS",
		"CARTRIDGE 2: MANDEL",
		"CARTRIDGE 3: PISTAZIE",
		"CARTRIDGE 4: ERDNUSSE",
		"CARTRIDGE 5: CASHEW",
		"CARTRIDGE 6: SEASAM"
	}
	chapterMenu.ru = {
		"КАРТРИДЖ 1: НУСС",
		"КАРТРИДЖ 2: МАНДЕЛЬ",
		"КАРТРИДЖ 3: ПИСТАЗИ",
		"КАРТРИДЖ 4: ЭРДНУСС",
		"КАРТРИДЖ 5: КЕШЬЮ",
		"КАРТРИДЖ 6: СИЗАМ"
	}
	chapterMenu.i = 1

	-- DEFAULTS
	ascii = love.graphics.newFont("fonts/joystix monospace.ttf",30)
	asciibig = love.graphics.newFont("fonts/joystix monospace.ttf",48)
	textfont = love.graphics.newFont("fonts/hardpixel.otf",36)
	
	if fastLevel == 0 then
		setColorScheme("default")
	else
		setChapterColorScheme()
	end

	--sounds and music
	correct = love.audio.newSource("sfx/correct.wav","static")
	wrong = love.audio.newSource("sfx/wrong.wav","static")
	selection = love.audio.newSource("sfx/selection.wav","static")
	intro = love.audio.newSource("sfx/intro.wav","static")
	step = love.audio.newSource("sfx/step.wav","static")
	shoo = love.audio.newSource("sfx/shoo.wav","static")
	talking = love.audio.newSource("sfx/talking.wav","static")

	-- title images
	pps = love.graphics.newImage("images/pps.png")
	headphones = love.graphics.newImage("images/headphones.png")
	
	love.window.setMode(770,560,{resizable = false, borderless = false, fullscreen = false, vsync = 1})
	love.window.setTitle("CASHEW")
	love.mouse.setVisible(false)
	
end

--██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗
--██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
--██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗  
--██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝  
--╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗
-- ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝

function engineUpdate()

	wh = love.graphics.getHeight()
	ww = love.graphics.getWidth()
	dw = ww/2
	dh = wh/2

	love.audio.setVolume(globalVolume/100)
	if hudGlobalVolume > globalVolume then
		hudGlobalVolume = hudGlobalVolume - 2
	elseif hudGlobalVolume < globalVolume then
		hudGlobalVolume = hudGlobalVolume + 2
	end

	hud.m1 = " "
	hud.pop = " "
	prevm2 = hud.m2
	prevm3 = hud.m3

	if hud.jumpState == 2 then
		if hud.jump > 0 then
			hud.jump = hud.jump - hud.jumpSpeed
		else
			hud.jump = 0
			hud.jumpState = 1
			freeze = false
		end
	end

	if ismoving == true and inventory.state == false and freeze == false and chapterIntro == false then
		ismoving = false
		if (mapN > 0) and (istalking == false) then
			if love.keyboard.isDown("up") or love.keyboard.isDown("w") then
					wasStep = true
					goUp()
			end
			if love.keyboard.isDown("down") or love.keyboard.isDown("s") then
					wasStep = true
					goDown()
			end
			if love.keyboard.isDown("right") or love.keyboard.isDown("d") then
					wasStep = true
					goRight()
			end
			if love.keyboard.isDown("left") or love.keyboard.isDown("a") then
					wasStep = true
					goLeft()
			end
			if player.floor == "$" then
				player.coins = player.coins + 1
				player.floor = " "
				play(talking)
			end
		end
		Timer.after(0.1,function()
			ismoving = true
		end)
	end

	-- MAPS LOGIC
	-- MENUS
	
	-- language menu
	if mapN == -5 then		
		if languageAtStartSwitch == -1 then
			if languageAtStartScale > 0 then
				languageAtStartScale = languageAtStartScale - 0.05	
			else
				languageAtStartScale = 0
				languageAtStartSwitch = 0
			end
		elseif languageAtStartSwitch == 1 then
			if languageAtStartScale < 1 then
				languageAtStartScale = languageAtStartScale + 0.05	
			else
				languageAtStartSwitch = 0
				mapN = -4
			end
		end
	end

	-- titles
	if mapN == -4 then

		-- beginning is for tiltes

		-- pause before
		if beginning == 1 then
			--Timer.after(1, function()
				if beginning == 1 then
					if mute == false then
						love.audio.play(intro)
					end
					beginning = 2
				end
			--end)
		end

		-- STUDIO LOGO FADEIN
		if beginning == 2 then
			if timer < 1 then
				timer = timer + 0.03
				opacity = timer
			end
			if timer > 1 then
				love.timer.sleep(2)
				beginning = 3
				timer = 0
			end
		end

		-- STUDIO LOGO FADEOUT
		if beginning == 3 then
			if timer < 1 then
				timer = timer + 0.03
				opacity = 1 - timer
			end
			if timer > 1 then
				beginning = 4
				timer = 0
			end
		end


		-- USE YOUR HEADPHONES FADEIN
		if beginning == 4 then
			if timer < 1 then
				timer = timer + 0.03
				opacity = timer
			end
			if timer > 1 then
				love.timer.sleep(2)
				beginning = 5
				timer = 0
			end
		end

		-- USE YOUR HEADPHONES FADEOUT
		if beginning == 5 then
			if timer < 1 then
				timer = timer + 0.03
				opacity = 1 - timer
			end
			if timer > 1 then
				beginning = 6
				timer = 0
				mapN = 0
			end
		end

		-- 
		if beginning == 0 then
			beginning = 1
		end
	end

	-- MAIN MENU
	if mapN == 0 then
		if beginning == 6 then
			if timer < 1 then
				timer = timer + 0.03
				opacity = timer
			end
			if timer > 1 then
				timer = 0
				if mute == false then
					love.audio.play(wrong)
				end
				beginning = -1
			end
		end
	end

	if mapN > 0 then

		--iterate timers for differents times

		--[[
		if global05 == true then
			print("global 0.5 true")
			global05 = false
			Timer.after(0.5, function()
				global05 = true
			end)
		end

		if global1 == true then
			print("global 1 true")
			global1 = false
			Timer.after(1, function()
				global1 = true
			end)
		end
		--]]

		if global2 == true then
			global2 = false
			-- global maps actions
			Timer.after(2, function()
				-- global afters
				for i = 1, mapSize do
					for j = 1, mapSize do
						if map[j][i] == "," then
							map[j][i] = " "
						elseif player.floor == "," then
							player.floor = " "
						end
					end
				end
				global2 = true
			end)
		end

		if player.floor == "$" or (player.floor == "!" and player.health < player.maxhealth) then
			if language == "en" then
				hud.pop = "press C"
			elseif language == "ru" then
				hud.pop = "нажми C"
			end
		end

		if player.floor == "~" and wasStep == true then
			player.health = player.health - 1
			play(wrong)
			wasStep = false
		end

		for i = 1, mapSize do
			for j = 1, mapSize do
				if map[i][j] == "x" then
					object(j,i,look,1)
				end
			end
		end

		--[[
		enemyMovement("a",1)
		enemyMovement("b",1)
		enemyMovement("c",1)
		enemyMovement("d",1)
		enemyMovement("e",1)
		enemyMovement("f",1)
		enemyMovement("g",1)
		enemyMovement("j",1)
		enemyMovement("k",1)
		enemyMovement("l",1)
		enemyMovement("m",1)
		enemyMovement("n",1)
		enemyMovement("o",1)
		enemyMovement("p",1)
		enemyMovement("q",1)
		enemyMovement("r",1)
		enemyMovement("s",1)
		enemyMovement("t",1)
		enemyMovement("u",1)
		enemyMovement("v",1)
		enemyMovement("w",1)
		enemyMovement("y",1)
		enemyMovement("z",0.3)
		enemyFighting()
		--]]

	end

	-- chapterIntros
	if chapterIntro == true and chapterMenuSwitch == 0 then
		if currentChapter == 1 then
			if introTip(intro, 1) then
				chapterIntro = false
				canBeContinued = true
			end
		end
	end

	-- animations
	animationsUI()

end

--███╗   ███╗ ██████╗ ██╗   ██╗███████╗███╗   ███╗███████╗███╗   ██╗████████╗
--████╗ ████║██╔═══██╗██║   ██║██╔════╝████╗ ████║██╔════╝████╗  ██║╚══██╔══╝
--██╔████╔██║██║   ██║██║   ██║█████╗  ██╔████╔██║█████╗  ██╔██╗ ██║   ██║   
--██║╚██╔╝██║██║   ██║╚██╗ ██╔╝██╔══╝  ██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║   
--██║ ╚═╝ ██║╚██████╔╝ ╚████╔╝ ███████╗██║ ╚═╝ ██║███████╗██║ ╚████║   ██║   
--╚═╝     ╚═╝ ╚═════╝   ╚═══╝  ╚══════╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝   

function engineMovement()

	function love.keypressed(key)

		if (mapN > 0) and (istalking == false) and (freeze == false) and (chapterIntro == false) then

			if inventory.state == false then
				if (key == "z") or (key == "j") then
					if defaults.chapter1.talkedWithUsher then
						play(wrong)
						inventoryswitch = 1
						inventoryscale = 0
					end
				end
			else
				if ((key == "z") or (key == "j")) and (inventoryswitch == 0) then
					play(correct)
					--inventory.state = false
					inventoryswitch = -1
				end
				local m
				if inventory.current.number < 5 then
					m = inventory.current.number
				else
					m = 5
				end
				if inventory.current.number > 0 then
					if (key == "down") or (key == "s") then
						if inventory.show.position == m then
							if inventory.current.position + m-1 < inventory.current.number then
								play(selection)
								inventory.current.position = inventory.current.position + 1
							end
						else
							play(selection)
							inventory.show.position = inventory.show.position + 1
						end
					end
					if (key == "up") or (key == "w") then
						if inventory.show.position == 1 then
							if inventory.current.position > 1 then
								play(selection)
								inventory.current.position = inventory.current.position - 1
							end
						else
							play(selection)
							inventory.show.position = inventory.show.position - 1
						end
					end
				end

			end

		end

		if (mapN == 0) and (beginning == -1) then

			if (key == "up") or (key == "w") then
				if menu.i == 1 then
					menu.i = #menu.en
				else
					menu.i = menu.i - 1
				end
				play(selection)
			end

			if (key == "down") or (key == "s") then
				if menu.i == #menu.en then
					menu.i = 1
				else
					menu.i = menu.i + 1
				end
				play(selection)
			end

			if ((key == "return") or (key == "c") or (key == "l")) and menuswitch == 0 then
				if menu.i == 1 then
					menuscale = 1
					menuswitch = 1
				end

				if menu.i == 2 then
					chapterMenuSwitch = 1
					chapterMenuScale = 1
					chapterMenu.i = 1
				end

				if menu.i == 4 then
					aboutSwitch = 1
					aboutScale = 1
					about.i = 1
				end

				if menu.i == 3 then
					settingsSwitch = 1
					settingsScale = 1
				end

				if menu.i == 5 then
					--saveAll()
					quitSwitch = 1
					quitScale = 1
				end

				play(correct)
			end

		end

		if (mapN == -5) then
			if (key == "up") or (key == "w") then
				if languageAtStart == 1 then
					languageAtStart = 2
				else
					languageAtStart = 1
				end
				play(selection)
			end

			if (key == "down") or (key == "s") then
				if languageAtStart == 2 then
					languageAtStart = 1
				else
					languageAtStart = 2
				end
				play(selection)
			end
			
			if (key == "return") or (key == "c") or (key == "l") then
				if languageAtStart == 1 then
					language = "en"
				elseif languageAtStart == 2 then
					language = "ru"
				end
				languageAtStartScale = 0
				languageAtStartSwitch = 1
			end
		end

		if (mapN == -2) then
			if (key == "up") or (key == "w") then
				if chapterMenu.i == 1 then
					if progress == 6 then
						chapterMenu.i = 6
						play(selection)
					end
				else
					chapterMenu.i = chapterMenu.i - 1
					play(selection)
				end
			end

			if (key == "down") or (key == "s") then
				if chapterMenu.i == 6 then
					chapterMenu.i = 1
					play(selection)
				else
					if progress >= chapterMenu.i + 1 then
						chapterMenu.i = chapterMenu.i + 1
						play(selection)
					end
				end
			end

			if (key == "c") or (key == "l") then
				if chapterMenu.i == 1 then
					currentChapter = 1
					chapterIntro = true
					chapterMenuSwitch = 3
				elseif chapterMenu.i == 2 then
					chapterMenuSwitch = 3
				elseif chapterMenu.i == 3 then
					chapterMenuSwitch = 3
				elseif chapterMenu.i == 4 then
					chapterMenuSwitch = 3
				elseif chapterMenu.i == 5 then
					chapterMenuSwitch = 3
				elseif chapterMenu.i == 6 then
					chapterMenuSwitch = 3
				end
			end
		end

		if (mapN == -3) then
			if (key == "up") or (key == "w") then
				if settingsMenu.i == 1 then
					settingsMenu.i = #settingsMenu.en
				else
					settingsMenu.i = settingsMenu.i - 1
				end
				play(selection)
			end

			if (key == "down") or (key == "s") then
				if settingsMenu.i == #settingsMenu.en then
					settingsMenu.i = 1
				else
					settingsMenu.i = settingsMenu.i + 1
				end
				play(selection)
			end

			if settingsMenu.i == 1 then
				if ((key == "left") or (key == "a")) and (globalVolume > 0) then
					globalVolume = globalVolume - 10
					play(selection)
				end
				if ((key == "right") or (key == "d")) and (globalVolume < 100) then
					globalVolume = globalVolume + 10
					play(selection)
				end
				if ((key == "c") or (key == "l")) and globalVolume < 100 then
					globalVolume = 100
					play(selection)
				end
			end

			if settingsMenu.i == 2 then
				if ((key == "left") or (key == "a") or (key == "right") or (key == "d") or (key == "c") or (key == "l")) and keyIsReadyToBePressed == true then
					keyIsReadyToBePressed = false
					if love.window.getFullscreen() then
						love.window.setFullscreen(false)
						love.window.setMode(770,560,{resizable = false, borderless = false, vsync = 1})
						effect.changeSize = true
						effect.crt.distortionFactor = {1.025, 1.025}
						effect.crt.feather = 0.2
					else
						love.window.setFullscreen(true)
						effect.changeSize = true
						effect.crt.distortionFactor = {1.05, 1.05}
						effect.crt.feather = 0.85
					end
					play(selection)
				end
			end

			if settingsMenu.i == 3 then
				if ((key == "left") or (key == "a") or (key == "right") or (key == "d") or (key == "c") or (key == "l")) and keyIsReadyToBePressed == true then
					keyIsReadyToBePressed = false
					if language == "en" then
						language = "ru"
					else
						language = "en"
					end
					play(selection)
				end
			end

			if settingsMenu.i == 4 then
				if ((key == "left") or (key == "a") or (key == "right") or (key == "d") or (key == "c") or (key == "l")) and keyIsReadyToBePressed == true then
					keyIsReadyToBePressed = false
					if effectIsEnabled == true then
						effectIsEnabled = false
						effect.disable("chromasep", "scanlines", "crt")
					else
						effectIsEnabled = true
						effect.enable("chromasep", "scanlines", "crt")
					end
					play(selection)
				end
			end

		end

		if (mapN == -1) then
			if (key == "up") or (key == "w") then
				if about.i > 1 then
					about.i = about.i - 1
					play(selection)
				end
			end
			if (key == "down") or (key == "s") then
				local aboutLen
					if language == "en" then aboutLen = #about.en
					elseif language == "ru" then aboutLen = #about.ru end
				if about.i < aboutLen - 5 then
					about.i = about.i + 1
					play(selection)
				end
			end
		end

	end

end

function engineKeyboard()
	function love.keyreleased(key)

		if keyIsReadyToBePressed == false then
			keyIsReadyToBePressed = true
		end

		if mapN > 0 then
			if key == "escape" and menuswitch == 0 and chapterIntro == false then
				play(wrong)
				--mapN = 0
				gamescale = 1
				menuswitch = -1
			end
		end

		if mapN == -1 then
			if key == "escape" or key == "x" or key == "k" then
				play(wrong)
				aboutSwitch = -1
			end
		end

		if mapN == -2 then
			if key == "escape" or key == "x" or key == "k" then
				play(wrong)
				chapterMenuSwitch = -1
			end
		end

		if mapN == -3 then
			if key == "escape" or key == "x" or key == "k" then
				play(wrong)
				settingsSwitch = -1
			end
		end

		if key == "f11" then
			if love.window.getFullscreen() then
				love.window.setFullscreen(false)
				love.window.setMode(770,560,{resizable = false, borderless = false, vsync = 1})
				effect.changeSize = true
				effect.crt.distortionFactor = {1.025, 1.025}
				effect.crt.feather = 0.2
			else
				love.window.setFullscreen(true)
				effect.changeSize = true
				effect.crt.distortionFactor = {1.05, 1.05}
				effect.crt.feather = 0.85
			end
		end

		if key == "f5" then
			if mute == false then
				mute = true
			else
				mute = false
			end
		end

		-- REMOVE IN BETA !!!
		if key == "f3" then
			if language == "en" then
				language = "ru"
			else
				language = "en"
			end
		end

		if key == "f1" then
			if effectIsEnabled == true then
				effectIsEnabled = false
				effect.disable("chromasep", "scanlines", "crt")
			else
				effectIsEnabled = true
				effect.enable("chromasep", "scanlines", "crt")
			end
		end
	end
end

--██████╗ ██████╗  █████╗ ██╗    ██╗
--██╔══██╗██╔══██╗██╔══██╗██║    ██║
--██║  ██║██████╔╝███████║██║ █╗ ██║
--██║  ██║██╔══██╗██╔══██║██║███╗██║
--██████╔╝██║  ██║██║  ██║╚███╔███╔╝
--╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚══╝╚══╝ 

function engineDraw()

	if mapN > 0 then

		if chapterIntro == true then

			love.graphics.setFont(ascii)
			love.graphics.setColor(white)
			graphics.print('@', 0, 0)

			love.graphics.setFont(textfont)
			--messagebox
			if dialogswitch ~= 0 or dialogscale == 1 then
				love.graphics.setColor(white)
				graphics.rectangle("line",-360,205-(65*dialogscale),720,130*dialogscale)
			end

			--messagetext
			if hud.m1 ~= " " then
				love.graphics.setColor(light)
				graphics.print(hud.m1,-350,150)
				love.graphics.setColor(white)
				graphics.print(hud.m2, -350, 190)
				graphics.print(hud.m3, -350, 220)
			end

			--arrow
			if hud.arrow == true then
				graphics.triangle("fill", 320, 225, 340, 225, 330, 240)
			end

			love.graphics.setColor(middle)
			graphics.print(hud.pop, 150, 64)

		else

			-- CAMERA OUTPUT

			if menuswitch == 0 then

				if player.x < 6 then
					if player.y < 6 then
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[j][i]
							end
						end
					elseif player.y > mapSize-6 then
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[mapSize-11+j][i]
							end
						end
					else
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[player.y-5+j][i]
							end
						end
					end
				elseif player.x > mapSize-6 then
					if player.y < 6 then
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[j][mapSize-11+i]
							end
						end
					elseif player.y > mapSize-6 then
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[mapSize-11+j][mapSize-11+i]
							end
						end
					else
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[player.y-5+j][mapSize-11+i]
							end
						end
					end
				else
					if player.y < 6 then
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[j][player.x-5+i]
							end
						end
					elseif player.y > mapSize-6 then
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[mapSize-11+j][player.x-5+i]
							end
						end
					else
						for i = 1, 11 do
							for j = 1, 11 do
								mapOut[j][i] = map[player.y-5+j][player.x-5+i]
							end
						end
					end
				end

				-- SCREEN
				love.graphics.setFont(textfont)
				love.graphics.setBackgroundColor(black)
				
				love.graphics.setColor(white)
				--mapbox
				graphics.rectangle("line",-121,-126,242,252)

				love.graphics.setColor(middle)
				if hud.pop == hud.prevPop and hud.popSwitch == 0 then
					hud.prevPop = hud.pop
				elseif hud.popSwitch == 0 then
					if hud.pop == " " then
						hud.popSwitch = -1
						hud.popScale = 1
					else
						hud.popSwitch = 1
						hud.popScale = 0
					end
				end

				if hud.popSwitch == 1 then --text appear
					if hud.popScale < 1 then
						hud.popScale = hud.popScale + 0.1
					else
						hud.popScale = 1
						hud.popSwitch = 0
						hud.prevPop = hud.pop
						black[4] = 1
					end
					graphics.print(hud.pop, 150, 64 + 20*hud.popScale)
					black[4] = 1 - hud.popScale
					love.graphics.setColor(black)
					graphics.rectangle("fill", 140, 64, 200, 70)
				elseif hud.popSwitch == -1 then --text disappear
					if hud.popScale > 0 then
						hud.popScale = hud.popScale - 0.1
					else
						hud.popScale = 0
						hud.popSwitch = 0
						hud.prevPop = hud.pop
						black[4] = 1
					end
					graphics.print(hud.prevPop, 150, 64 + 20*hud.popScale)
					black[4] = 1 - hud.popScale
					love.graphics.setColor(black)
					graphics.rectangle("fill", 140, 64, 200, 70)
				else --(hud.popSwitch == 0)
					graphics.print(hud.pop, 150, 84)
				end

				love.graphics.setFont(ascii)
				for i = 1,11 do
					for j = 1,11 do
						love.graphics.setColor(middle)
						if mapOut[j][i] == "@" then
							if player.floor == "*" and tipIsComplete == false then
								love.graphics.setColor(light)
							else
								love.graphics.setColor(white)
							end
						elseif mapOut[j][i] == "*" then love.graphics.setColor(light)
						elseif mapOut[j][i] == "?" then love.graphics.setColor(light)
						elseif mapOut[j][i] == "x" then love.graphics.setColor(light)
						elseif mapOut[j][i] == "," then love.graphics.setColor(light)
						elseif checkEnemy(mapOut[j][i]) then love.graphics.setColor(light)
						elseif checkDark(mapOut[j][i]) then love.graphics.setColor(dark) end
						--if checkEnemy(map[j][i]) then love.graphics.setColor(white) end
						graphics.print(mapOut[j][i],-131+i*20,-140+j*20)
						if (i == 1 or i == 11 or j == 1 or j == 11) then
							black[4] = 0.75-lighter
						elseif (i == 2 or i == 10 or j == 2 or j == 10) then
							black[4] = 0.5-lighter
						elseif (i == 3 or i == 9 or j == 3 or j == 9) then
							black[4] = 0.25-lighter
						elseif lighter < 0 then
							black[4] = -ligter
						else
							black[4] = 0
						end
						love.graphics.setColor(black)
						graphics.print(mapOut[j][i],-131+i*20,-140+j*20)
						black[4] = 1
					end
				end

				local c = {}
				c[1] = black[1]
				c[2] = black[2]
				c[3] = black[3]
				c[4] = hud.jump

				love.graphics.setColor(c)
				graphics.rectangle("fill",-120,-125,240,250)

				local c = {}
				c[1] = black[1]
				c[2] = black[2]
				c[3] = black[3]
				c[4] = hud.allblack

				love.graphics.setColor(c)
				graphics.rectangle("fill",-ww/2,-wh/2,ww,wh)

				love.graphics.setFont(textfont)
				--messagebox
				if dialogswitch ~= 0 or dialogscale == 1 then
					love.graphics.setColor(white)
					graphics.rectangle("line",-360,205-(65*dialogscale),720,130*dialogscale)
				end

				--messagetext
				love.graphics.setColor(light)
				graphics.print(hud.m1,-350,150)
				love.graphics.setColor(white)
				graphics.print(hud.m2, -350, 190)
				graphics.print(hud.m3, -350, 220)

				--arrow
				if hud.arrow == true then
					graphics.triangle("fill", 320, 225, 340, 225, 330, 240)
				end

				--inventory
				if inventoryswitch == 1 or inventoryswitch == -1 then
					love.graphics.setColor(black)
					graphics.rectangle("fill",-360,-200*inventoryscale,720,400*inventoryscale)
					love.graphics.setColor(white)
					graphics.rectangle("line",-360,-200*inventoryscale,720,400*inventoryscale)
				end

				if inventory.state == true and inventoryswitch ~= -1 then
					local i
					local m
					local langNum

					if language == "en" then
						langNum = 1
					elseif language == "ru" then
						langNum = 2
					end

					if inventory.current.number < 5 then
						m = inventory.current.number
					else
						m = 5
					end
					for i = 1, m do
						inventory.show[i] = inventory.current[inventory.current.position-1+i]
					end
					
					love.graphics.setColor(black)
					graphics.rectangle("fill",-360,-200,720,400)
					love.graphics.setColor(white)
					graphics.rectangle("line",-360,-200,720,400)
					love.graphics.line(0,-180, 0,180)

					love.graphics.setColor(middle)
					local rightMarginCoin = 0
					if player.coins > 9999 then rightMarginCoin = 80
					elseif player.coins > 999 then rightMarginCoin = 60
					elseif player.coins > 99 then rightMarginCoin = 40
					elseif player.coins > 9 then rightMarginCoin = 20 end
					if language == "en" then
						graphics.print("INVENTORY",-360,-260)
						graphics.print("COINS: "..player.coins, 210-rightMarginCoin, -260)
					else
						graphics.print("ИНВЕНТАРЬ",-360,-260)
						graphics.print("МОНЕТЫ: "..player.coins, 160-rightMarginCoin, -260)
					end

					if inventory.current.number > 0 then

						love.graphics.setColor(white)

						for i = 1, m do
							graphics.print(inventory.show[i][langNum][1],-300,-200+i*60)
						end

						local p = -180+inventory.show.position*60
						graphics.triangle("fill", -330, p-10, -330, p+10, -315, p)

						if language == "en" then
							graphics.print("Description:",40,-180)
						elseif language == "ru" then
							graphics.print("Описание:",40,-180)
						end

						love.graphics.setColor(light)
						graphics.print(inventory.show[inventory.show.position][langNum][2][1],20,-120)
						graphics.print(inventory.show[inventory.show.position][langNum][2][2],20,-80)
						graphics.print(inventory.show[inventory.show.position][langNum][2][3],20,-40)
						graphics.print(inventory.show[inventory.show.position][langNum][2][4],20,0)
						graphics.print(inventory.show[inventory.show.position][langNum][2][5],20,40)
						graphics.print(inventory.show[inventory.show.position][langNum][2][6],20,80)
						love.graphics.setColor(middle)
						graphics.print(inventory.show[inventory.show.position][langNum][2][7],20,140)

					else
						graphics.print("-no items-",-300,-140)
					end

					love.graphics.setColor(middle)
					if language == "en" then
						graphics.print("Z - back",-360,220)
					elseif language == "ru" then
						graphics.print("Z - назад",-360,220)
					end
				end
			else
				love.graphics.setColor(white)
				graphics.rectangle("line",-121,-126*gamescale,242,252*gamescale)
			end

		end
		
	elseif mapN == 0 then -- main menu
		love.graphics.setBackgroundColor(black)
		love.graphics.setColor(white)
		if quitSwitch ~= 0 then
			graphics.rectangle("line",-200,-55*quitScale,400,240*quitScale)
		elseif settingsSwitch ~= 0 then
			graphics.rectangle("line",-200,-55*settingsScale,400,240*settingsScale)
		elseif aboutSwitch ~= 0 then
			graphics.rectangle("line",-200,-55*aboutScale,400,240*aboutScale)
		elseif chapterMenuSwitch ~= 0 then
			graphics.rectangle("line",-200,-55*chapterMenuScale,400,240*chapterMenuScale)
		elseif menuswitch ~= 0 then
			graphics.rectangle("line",-200,-55*menuscale,400,240*menuscale)
		else -- normal view

			if beginning == 6 then

				love.graphics.setFont(asciibig)
				graphics.print("CASHEW",-120,-160+timer*25)
				graphics.rectangle("line",-200,-80+timer*25,400,240)
				love.graphics.setFont(textfont)
				for i = 1, #menu.en do
					if i == menu.i then
						love.graphics.setColor(white)
						if language == "en" then
							graphics.triangle("fill", -110, -90+timer*25+i*40, -110, -70+timer*25+i*40, -95, -80+timer*25+i*40)
						elseif language == "ru" then
							graphics.triangle("fill", -125, -90+timer*25+i*40, -125, -70+timer*25+i*40, -110, -80+timer*25+i*40)
						end
					else
						love.graphics.setColor(middle)
					end
					if language == "en" then
						graphics.print(menu.en[i],-85,-100+timer*25+i*40)
					elseif language == "ru" then
						graphics.print(menu.ru[i],-100,-100+timer*25+i*40)
					end
				end

			elseif beginning == -1 then

				--graphics.print("030419 (UI TEST BUILD)",-ww/2+20,-wh/2+20)

				love.graphics.setFont(asciibig)
				graphics.print("CASHEW",-120,-135)
				graphics.rectangle("line",-200,-55,400,240)

				love.graphics.setFont(textfont)

				love.graphics.setColor(middle)
				if language == "en" then
					graphics.print("C - choose", -200, 190)
				elseif language == "ru" then
					graphics.print("C - выбрать", -200, 190)
				end

				for i = 1, #menu.en do
					if i == menu.i then
						love.graphics.setColor(white)
						if language == "en" then
							graphics.triangle("fill", -110, -65+i*40, -110, -45+i*40, -95, -55+i*40)
						elseif language == "ru" then
							graphics.triangle("fill", -125, -65+i*40, -125, -45+i*40, -110, -55+i*40)
						end
					else
						love.graphics.setColor(middle)
					end
					if language == "en" then
						graphics.print(menu.en[i],-85,-75+i*40)
					elseif language == "ru" then
						graphics.print(menu.ru[i],-100,-75+i*40)
					end
				end

			end

			changeColor = {black[1],black[2],black[3],1 - opacity}
			love.graphics.setColor(changeColor)
			graphics.rectangle("fill",-ww/2,-wh/2,ww,wh)

		end

	elseif mapN == -1 then -- about menu

		love.graphics.setBackgroundColor(black)
		love.graphics.setFont(textfont)
		love.graphics.setColor(white)

		if aboutSwitch == 0 then
			-- frame
			graphics.rectangle("line",-300,-180,600,360)

			-- text
			love.graphics.setColor(middle)

			if language == "en" then
				graphics.print("ABOUT", -300, -230)
			elseif language == "ru" then
				graphics.print("ОБ ИГРЕ", -300, -230)
			end

			if language == "en" then
				graphics.print("X - back", -300, 180)
			elseif language == "ru" then
				graphics.print("X - назад", -300, 180)
			end

			if language == "en" then
				for i = 1, 6 do
					love.graphics.setColor(light)
					about.show[i] = about.en[about.i + i - 1]
					graphics.print(about.show[i], -220, -180 + 40*i)
					love.graphics.setColor(white)
					if about.i > 1 then
						graphics.triangle("fill", -270, -120, -260, -135, -250, -120)
					end
					if about.i < #about.en - 5 then
						graphics.triangle("fill", -270, 120, -260, 135, -250, 120)
					end
				end
			elseif language == "ru" then
				for i = 1, 6 do
					love.graphics.setColor(light)
					about.show[i] = about.ru[about.i + i - 1]
					graphics.print(about.show[i], -220, -180 + 40*i)
					love.graphics.setColor(white)
					if about.i > 1 then
						graphics.triangle("fill", -270, -120, -260, -135, -250, -120)
					end
					if about.i < #about.ru - 5 then
						graphics.triangle("fill", -270, 120, -260, 135, -250, 120)
					end
				end
			end

		else
			graphics.rectangle("line",-300,-180*aboutScale,600,360*aboutScale)
		end

	elseif mapN == -2 then -- chapters menu

		love.graphics.setBackgroundColor(black)
		love.graphics.setFont(textfont)
		love.graphics.setColor(white)

		if chapterMenuSwitch == 0 then
			-- frame
			graphics.rectangle("line", -250, -180, 500, 360)

			-- text
			for i = 1, 6 do

				if chapterMenu.i == i then
					love.graphics.setColor(white)
				elseif progress >= i then
					love.graphics.setColor(middle)
				else
					love.graphics.setColor(dark)
				end

				if language == "en" then
					graphics.print(chapterMenu.en[i], -180, -180 + 40*i)
				elseif language == "ru" then
					graphics.print(chapterMenu.ru[i], -180, -180 + 40*i)
				end

				love.graphics.setColor(white)
				if chapterMenu.i == i then
					graphics.triangle("fill", -210, -170 + 40*i, -210, -150 + 40*i, -195, -160 + 40*i)
				end

			end

			love.graphics.setColor(middle)

			if language == "en" then
				graphics.print("CARTRIDGES", -250, -230)
			elseif language == "ru" then
				graphics.print("КАРТРИДЖИ", -250, -230)
			end

			if language == "en" then
				graphics.print("C - choose", -250, 180)
				graphics.print("X - back", -250, 220)
			elseif language == "ru" then
				graphics.print("C - выбрать", -250, 180)
				graphics.print("X - назад", -250, 220)
			end

		else
			graphics.rectangle("line",-250,-180*chapterMenuScale,500,360*chapterMenuScale)
		end

	elseif mapN == -3 then -- settings menu
		
		love.graphics.setBackgroundColor(black)
		love.graphics.setFont(textfont)
		
		love.graphics.setColor(white)

		if settingsSwitch == 0 then

			graphics.rectangle("line",-300,-180,600,360)

			local listOut

			for i = 1, #settingsMenu.en do
				if language == "en" then
					if settingsMenu.en[i] == "VOLUME" then
						listOut = settingsMenu.en[i]..": "..globalVolume.."%"
					elseif settingsMenu.en[i] == "FULLSCREEN" then
						if love.window.getFullscreen() then
							listOut = settingsMenu.en[i]..": ON"
						else
							listOut = settingsMenu.en[i]..": OFF"
						end
					elseif settingsMenu.en[i] == "LANGUAGE" then
						listOut = settingsMenu.en[i] .. ": ENGLISH"
					elseif settingsMenu.en[i] == "RETRO MODE" then
						if effectIsEnabled == true then
							listOut = settingsMenu.en[i] .. ": ON"
						else
							listOut = settingsMenu.en[i] .. ": OFF"
						end
					else
						listOut = settingsMenu.en[i]
					end

					love.graphics.setColor(white)
					graphics.print(listOut, -200, -240 + 80*i)

				elseif language == "ru" then
					if settingsMenu.ru[i] == "ГРОМКОСТЬ" then
						listOut = settingsMenu.ru[i]..": "..globalVolume.."%"
					elseif settingsMenu.ru[i] == "ПОЛНЫЙ ЭКРАН" then
						if love.window.getFullscreen() then
							listOut = settingsMenu.ru[i]..": ВКЛ"
						else
							listOut = settingsMenu.ru[i]..": ВЫКЛ"
						end
					elseif settingsMenu.ru[i] == "ЯЗЫК" then
						listOut = settingsMenu.ru[i] .. ": РУССКИЙ"
					elseif settingsMenu.ru[i] == "РЕТРО РЕЖИМ" then
						if effectIsEnabled == true then
							listOut = settingsMenu.ru[i] .. ": ВКЛ"
						else
							listOut = settingsMenu.ru[i] .. ": ВЫКЛ"
						end
					else
						listOut = settingsMenu.ru[i]
					end

					love.graphics.setColor(white)
					graphics.print(listOut, -200, -240 + 80*i)
				end

				-- volume bar
				love.graphics.setColor(white)
				graphics.rectangle("line", -200, -110, 400, 20)
				graphics.rectangle("fill", -200, -110, 400 * hudGlobalVolume/100, 20)

				-- fullscreen icons
				if love.window.getFullscreen() then
					love.graphics.setColor(middle)
					graphics.rectangle("line", 150, -30, 30, 30)
					graphics.rectangle("fill", 150, -30, 30, 7)
					love.graphics.setColor(white)
					graphics.rectangle("line", 200, -30, 50, 30)
				else
					love.graphics.setColor(white)
					graphics.rectangle("line", 150, -30, 30, 30)
					graphics.rectangle("fill", 150, -30, 30, 7)
					love.graphics.setColor(middle)
					graphics.rectangle("line", 200, -30, 50, 30)
				end

				love.graphics.setColor(middle)
				-- up text
				if language == "en" then
					graphics.print("SETTINGS", -300, -230)
				elseif language == "ru" then
					graphics.print("НАСТРОЙКИ", -300, -230)
				end

				-- down text
				if language == "en" then
					if settingsMenu.i ~= 1 then
						graphics.print("C - change", -300, 180)
					else
						graphics.print("ARROWS - change", -300, 180)
					end
					graphics.print("X - back", -300, 220)
				elseif language == "ru" then
					if settingsMenu.i ~= 1 then
						graphics.print("C - изменить", -300, 180)
					else
						graphics.print("СТРЕЛКИ - изменить", -300, 180)
					end
					graphics.print("X - назад", -300, 220)
				end

				love.graphics.setColor(white)
				if settingsMenu.i == i then
						graphics.triangle("fill", -230, -230 + 80*i, -230, -210 + 80*i, -215, -220 + 80*i)
				end

			end
		else
			-- else for animation in settings
			graphics.rectangle("line",-300,-180*settingsScale,600,360*settingsScale)
		end

		elseif mapN == -4 then -- begginging titles

		love.graphics.setBackgroundColor(black)
		love.graphics.setFont(textfont)
		love.graphics.setColor(white)

		if beginning == 2 then
			graphics.draw(pps,-140,-160+timer*25)
			--graphics.draw(nr,-140,-120+timer*25)
			if language == "en" then
				graphics.print("POKATO POCO SHOW PRESENTS",-252,60+timer*25)
			elseif language == "ru" then
				graphics.print("POKATO POCO SHOW ПРЕДСТАВЛЯЕТ",-312,60+timer*25)
			end
		elseif beginning == 3 then
			graphics.draw(pps,-140,-160+25+timer*25)
			--graphics.draw(nr,-140,-120+25+timer*25)
			if language == "en" then
				graphics.print("POKATO POCO SHOW PRESENTS",-252,60+25+timer*25)
			elseif language == "ru" then
				graphics.print("POKATO POCO SHOW ПРЕДСТАВЛЯЕТ",-312,60+25+timer*25)
			end
		elseif beginning == 4 then
			graphics.draw(headphones,-70,-120+timer*25)
			if language == "en" then
				graphics.print("   USE YOUR HEADPHONES",-252,60+timer*25)
				graphics.print("        (OR DON'T)",-252,100+timer*25)
			elseif language == "ru" then
				graphics.print("   ИСПОЛЬЗУЙ НАУШНИКИ",-252,60+timer*25)
				graphics.print("       (НУ ИЛИ НЕТ)",-252,100+timer*25)
			end
		elseif beginning == 5 then
			graphics.draw(headphones,-70,-120+25+timer*25)
			if language == "en" then
				graphics.print("   USE YOUR HEADPHONES",-252,60+25+timer*25)
				graphics.print("        (OR DON'T)",-252,100+25+timer*25)
			elseif language == "ru" then
				graphics.print("   ИСПОЛЬЗУЙ НАУШНИКИ",-252,60+25+timer*25)
				graphics.print("       (НУ ИЛИ НЕТ)",-252,100+25+timer*25)
			end
		end
	
		changeColor = {black[1],black[2],black[3],1 - opacity}
		love.graphics.setColor(changeColor)
		graphics.rectangle("fill",-dw,-dh,ww,wh)

	elseif mapN == -5 then -- choose language menu at start

		love.graphics.setBackgroundColor(black)
		love.graphics.setFont(textfont)
		
		local delta = 0 --DEFAULT STILL
		if languageAtStartSwitch == -1 then --IN
			delta = - 25 * languageAtStartScale
		elseif languageAtStartSwitch == 1 then --OUT
			delta = 25 * languageAtStartScale
		end
		
		if languageAtStart == 1 then love.graphics.setColor(white) else love.graphics.setColor(middle) end
		graphics.print("ENGLISH", -70, -45+delta)
		if languageAtStart == 1 then love.graphics.setColor(middle) else love.graphics.setColor(white) end
		graphics.print("РУССКИЙ", -70, -5+delta)
		love.graphics.setColor(white)
		if languageAtStart == 1 then
			graphics.triangle("fill", -90, -30 + delta, -90, -10 + delta, -75, -20 + delta)
			--REMOVE
			graphics.print("ENGLISH IS NOT READY\nCHOOSE RUSSIAN",-ww/2+20,-wh/2+20)
		elseif languageAtStart == 2 then
			graphics.triangle("fill", -90, 30 + delta, -90, 10 + delta, -75, 20 + delta)
		end
		graphics.rectangle("line", -100, -50 + delta, 200, 100)
		
		changeColor = {black[1],black[2],black[3],languageAtStartScale}
		love.graphics.setColor(changeColor)
		graphics.rectangle("fill",-dw,-dh,ww,wh)
		
	end

end
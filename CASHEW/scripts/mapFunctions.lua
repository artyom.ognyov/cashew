-- FUNCTIONS FOR MAP OPERATIONS

function jump(currentX,currentY,currentM,nextM,newN,pX,pY)
	if player.x == currentX and player.y == currentY then
		if hud.jump == 0 then
			--hud.jumpState = 1
			freeze = true
			play(wrong)
			hud.jump = hud.jump + hud.jumpSpeed
		elseif hud.jump < 1 and hud.jump > 0 then
			hud.jump = hud.jump + hud.jumpSpeed
		else
			map[player.y][player.x] = player.floor
			currentM = mapN
			map = nextM
			mapSize = checkMapSize(map)
			player.x = pX
			player.y = pY
			player.floor = " "
			map[player.y][player.x] = "@"
			currentLevel = newN
			mapN = currentLevel
			hud.jumpState = 2
			-- end of animation in update function
		end
	end
end

function forcedJump(nextM, nextN, pX, pY)
		if hud.jump == 0 then
			--hud.jumpState = 1
			freeze = true
			play(wrong)
			hud.jump = hud.jump + hud.jumpSpeed
		elseif hud.jump < 1 and hud.jump > 0 then
			hud.jump = hud.jump + hud.jumpSpeed
		else
			map = nextM
			mapSize = checkMapSize(map)
			player.x = pX
			player.y = pY
			player.floor = " "
			map[player.y][player.x] = "@"
			currentLevel = newN
			mapN = currentLevel
			hud.jumpState = 2
			-- end of animation in update function
		end
end

function getCell(x,y,what)
	local f = false
	if x < mapSize and y < mapSize then
		if map[y][x] == what then
			f = true
		end
	end
	return f
end

function setCell(x,y,what)
	if x < mapSize and y < mapSize then
		map[y][x] = what
	end
end

function checkButton(x,y)
	local pressed = false
	if getCell(x,y,"#") or getCell(x,y,"@") then
		pressed = true
	else
		map[y][x] = "O"
	end
	return pressed
end

function checkSolid(char)
	local check = false
	for i = 1, #solidAre do
		if char == string.sub(solidAre, i, i) then
			check = true
		end
	end
	return check
end

function checkSolidAndPlayer(char)
	local check = false
	for i = 1, #solidAre do
		if char == string.sub(solidAre, i, i) then
			check = true
		end
	end
	if char == "@" then
		check = true
	end
	return check
end

function checkMovable(char)
	local check = false
	for i = 1, #movableAre do
		if char == string.sub(movableAre, i, i) then
			check = true
		end
	end
	return check
end

function checkEnemy(char)
	local check = false
	for i = 1, #enemiesAre do
		if char == string.sub(enemiesAre, i, i) then
			check = true
		end
	end
	return check
end

function checkDark(char)
	local check = false
	for i = 1, #darkAre do
		if char == string.sub(darkAre, i, i) then
			check = true
		end
	end
	return check
end

-- MOVEMENT
function goUp()
	if not checkSolid(map[player.y-1][player.x]) then
		if player.floor ~= "*" then
			map[player.y][player.x] = player.floor
		else
			map[player.y][player.x] = " "
		end
		player.floor = map[player.y-1][player.x]
		if mute == false then step:play() end
		player.y = player.y - 1
		map[player.y][player.x] = '@'
	elseif checkMovable(map[player.y-1][player.x]) then
		if not checkSolid(map[player.y-2][player.x]) and map[player.y-2][player.x] ~= ">" and map[player.y-2][player.x] ~= "<" then
			map[player.y][player.x] = player.floor
			player.y = player.y - 1
			local movableFloor = map[player.y][player.x]
			map[player.y][player.x] = "@"
			if movableFloor ~= "O" then
				map[player.y-1][player.x] = movableFloor
			else
				map[player.y-1][player.x] = " "
			end
			if mute == false then play(shoo) end
		else
			map[player.y][player.x] = map[player.y-1][player.x]
			player.y = player.y - 1
			map[player.y][player.x] = "@"
			if mute == false then play(shoo) end
		end
	end
	-- tip is complete
	if tipIsComplete == true then tipIsComplete = false end
	if forcedDialog == true then forcedDialog = false end
end

function goDown()
	if not checkSolid(map[player.y+1][player.x]) then
		if player.floor ~= "*" then
			map[player.y][player.x] = player.floor
		else
			map[player.y][player.x] = " "
		end
		player.floor = map[player.y+1][player.x]
		if mute == false then step:play() end
		player.y = player.y + 1
		map[player.y][player.x] = '@'
	elseif checkMovable(map[player.y+1][player.x]) then
		if not checkSolid(map[player.y+2][player.x]) and map[player.y+2][player.x] ~= ">" and map[player.y+2][player.x] ~= "<" then
			map[player.y][player.x] = player.floor
			player.y = player.y + 1
			local movableFloor = map[player.y][player.x]
			map[player.y][player.x] = "@"
			if movableFloor ~= "O" then
				map[player.y+1][player.x] = movableFloor
			else
				map[player.y+1][player.x] = " "
			end
			if mute == false then play(shoo) end
		else
			map[player.y][player.x] = map[player.y+1][player.x]
			player.y = player.y + 1
			map[player.y][player.x] = "@"
			if mute == false then play(shoo) end
		end
	end
	-- tip is complete
	if tipIsComplete == true then tipIsComplete = false end
	if forcedDialog == true then forcedDialog = false end
end

function goRight()
	if not checkSolid(map[player.y][player.x+1]) then
		if player.floor ~= "*" then
			map[player.y][player.x] = player.floor
		else
			map[player.y][player.x] = " "
		end
		player.floor = map[player.y][player.x+1]
		if mute == false then step:play() end
		player.x = player.x + 1
		map[player.y][player.x] = '@'
	elseif checkMovable(map[player.y][player.x+1]) then
		if not checkSolid(map[player.y][player.x+2]) and map[player.y][player.x+2] ~= ">" and map[player.y][player.x+2] ~= "<" then
			map[player.y][player.x] = player.floor
			player.x = player.x + 1
			local movableFloor = map[player.y][player.x]
			map[player.y][player.x] = "@"
			if movableFloor ~= "O" then
				map[player.y][player.x+1] = movableFloor
			else
				map[player.y][player.x+1] = " "
			end
			if mute == false then play(shoo) end
		else
			map[player.y][player.x] = map[player.y][player.x+1]
			player.x = player.x + 1
			map[player.y][player.x] = "@"
			if mute == false then play(shoo) end
		end
	end
	-- tip is complete
	if tipIsComplete == true then tipIsComplete = false end
	if forcedDialog == true then forcedDialog = false end
end

function goLeft()
	if not checkSolid(map[player.y][player.x-1]) then
		if player.floor ~= "*" then
			map[player.y][player.x] = player.floor
		else
			map[player.y][player.x] = " "
		end
		player.floor = map[player.y][player.x-1]
		if mute == false then step:play() end
		player.x = player.x - 1
		map[player.y][player.x] = '@'
	elseif checkMovable(map[player.y][player.x-1]) then
		if not checkSolid(map[player.y][player.x-2]) and map[player.y][player.x-2] ~= ">" and map[player.y][player.x-2] ~= "<" then
			map[player.y][player.x] = player.floor
			player.x = player.x - 1
			local movableFloor = map[player.y][player.x]
			map[player.y][player.x] = "@"
			if movableFloor ~= "O" then
				map[player.y][player.x-1] = movableFloor
			else
				map[player.y][player.x-1] = " "
			end
			if mute == false then play(shoo) end
		else
			map[player.y][player.x] = map[player.y][player.x-1]
			player.x = player.x - 1
			map[player.y][player.x] = "@"
			if mute == false then play(shoo) end
		end
	end
	-- tip is complete
	if tipIsComplete == true then tipIsComplete = false end
	if forcedDialog == true then forcedDialog = false end
end

-- SETS MAP TO CURRENT MAP (FOR FAST CHANGE) 
function setCurrentMap()
	local localMap
	if currentChapter == 1 then
		if currentLevel == 1 then		localMap = chapter1.map1
		elseif currentLevel == 2 then	localMap = chapter1.map2
		elseif currentLevel == 3 then	localMap = chapter1.map3
		elseif currentLevel == 4 then	localMap = chapter1.map4
		elseif currentLevel == 5 then	localMap = chapter1.map5
		elseif currentLevel == 6 then	localMap = chapter1.map6
		elseif currentLevel == 7 then	localMap = chapter1.map7
		elseif currentLevel == 8 then	localMap = chapter1.map8
		elseif currentLevel == 9 then	localMap = chapter1.map9
		end
	elseif currentChapter == 2 then
		if currentLevel == 1 then		localMap = chapter1.map1
		elseif currentLevel == 2 then	localMap = chapter1.map2
		end
	elseif currentChapter == 3 then
		if currentLevel == 1 then		localMap = chapter1.map1
		elseif currentLevel == 2 then	localMap = chapter1.map2
		end
	elseif currentChapter == 4 then
		if currentLevel == 1 then		localMap = chapter1.map1
		elseif currentLevel == 2 then	localMap = chapter1.map2
		end
	elseif currentChapter == 5 then
		if currentLevel == 1 then		localMap = chapter1.map1
		elseif currentLevel == 2 then	localMap = chapter1.map2
		end
	elseif currentChapter == 6 then
		if currentLevel == 1 then		localMap = chapter1.map1
		elseif currentLevel == 2 then	localMap = chapter1.map2
		end

	end
	
	return localMap
end
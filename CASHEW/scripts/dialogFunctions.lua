-- FUNCTIONS FOR DIALOGS
require ("utf8")

notCyrillic = '1234567890 -.,!?|/"@#$%^&*(){}[]abcdefghigklmnopqrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ'

function checkNotCyrillicSymbol(s)
	local x = false
	for y = 1, #notCyrillic do
		if s == string.sub(notCyrillic,y,y) then
			x = true
		end
	end
	return x
end

function dialogEnter(dialog, num)

	local m = false

	if tipIsComplete == false then
		if language == "en" and hud.pop ~= "press Z" then
			hud.pop = "press C"
		elseif language == "ru" and hud.pop ~= "нажми Z"  then
			hud.pop = "нажми C"
		end
	end
	if istalking == false then -- if not talking now then
		if (love.keyboard.isScancodeDown("c","l") or forcedDialog == true)
				and inventory.state == false
				and dialogswitch == 0
				and keyIsReadyToBePressed == true
				and tipIsComplete == false then
			-- when pressing the key and not inventory and not using dialog animation and keyIsReadyToBePressed
			forcedDialog = false
			if keyIsReadyToBePressed == false then keyIsReadyToBePressed = false end
			if n == 0 then
				n = 1
				istalking = true
				dialogswitch = 1
				numDialog = num
				if numDialog == 0 then
					if language == "en" then
						numDialog = love.math.random(1, #dialog.en)
					elseif language == "ru" then
						numDialog = love.math.random(1, #dialog.ru)
					end
				end
			end
		end
	end

	if n > 0 then
		if language == "en" then
			m = talk(dialog.en, numDialog)
		elseif language == "ru" then
			m = talk(dialog.ru, numDialog)
		end
	end
	
	return m

end


function talk(dialog,num)

	local p = false
	
	if dialogswitch == 0 then

		if num == 0 then
			num = love.math.random(1, #dialog)
		end

		if n == #dialog[num] then
			hud.arrow = false
		end

		if talkingLetters == 0 then
			stringCount = 0
			talkingLetters = 1
		end

		local str1 = 2
		local str2 = 3

		if #dialog[num][n] > 2 then
			hud.m1 = dialog[num][n][1]
		else
			hud.m1 = dialog.name
			str1 = 1
			str2 = 2
		end
		
		if talkingLetters == 1 then
			if stringCount <= #dialog[num][n][str1] then
				if hud.m2 == " " then
					if checkNotCyrillicSymbol(string.sub(dialog[num][n][str1],1,1)) then
						hud.m2 = string.sub(dialog[num][n][str1],1,1)
						stringCount = 2
					else
						hud.m2 = string.sub(dialog[num][n][str1],1,2)
						stringCount = 3
					end
				else
					if checkNotCyrillicSymbol(string.sub(dialog[num][n][str1], stringCount, stringCount)) then
						hud.m2 = hud.m2..string.sub(dialog[num][n][str1], stringCount, stringCount)
						stringCount = stringCount + 1
					else
						hud.m2 = hud.m2..string.sub(dialog[num][n][str1], stringCount, stringCount+1)
						stringCount = stringCount + 2
					end
				end
				if mute == false then
					talking:play()
				end
			else
				stringCount = 0
				talkingLetters = 2
			end
		end

		if talkingLetters == 2 then
			if stringCount <= #dialog[num][n][str2] then
				if hud.m3 == " " then
					if checkNotCyrillicSymbol(string.sub(dialog[num][n][str2],1,1)) then
						hud.m3 = string.sub(dialog[num][n][str2],1,1)
						stringCount = 2
					else
						hud.m3 = string.sub(dialog[num][n][str2],1,2)
						stringCount = 3
					end
				else
					if checkNotCyrillicSymbol(string.sub(dialog[num][n][str2], stringCount, stringCount)) then
						hud.m3 = hud.m3..string.sub(dialog[num][n][str2], stringCount, stringCount)
						stringCount = stringCount + 1
					else
						hud.m3 = hud.m3..string.sub(dialog[num][n][str2], stringCount, stringCount+1)
						stringCount = stringCount + 2
					end
				end
				if mute == false then
					talking:play()
				end
			else
				--Timer.after(0.2, function()
						talkingLetters = 3
				--end)
			end
		end

		if talkingLetters == 3 then
			if n < #dialog[num] then
				hud.arrow = true
			end
			if love.keyboard.isScancodeDown("c",'l') and keyIsReadyToBePressed == true then
					keyIsReadyToBePressed = false
					talkingLetters = 0
					if n < #dialog[num] then
						n = n + 1
						hud.m2 = " "
						hud.m3 = " "
					else
						n = 0
						hud.m1 = " "
						hud.m2 = " "
						hud.m3 = " "
						p = true
						dialogswitch = -1
						tipIsComplete = true
					end
				newInput = false
			elseif love.keyboard.isScancodeDown("x",'k') and chapterIntro == false then
				talkingLetters = 0
				n = 0
				hud.m1 = " "
				hud.m2 = " "
				hud.m3 = " "
				dialogswitch = -1
				hud.arrow = false
				tipIsComplete = true
			end
		end
		
	end
	
	return p
end
-- FUNCTIONS FOR MAP OBJECTS GAME LOGIC

function examine(currentX, currentY, dialog, num)
	if player.x == currentX and player.y == currentY then
		dialogEnter(dialog,num)
	end
end

function object(objectX, objectY, dialog, num)
	local b = false
	if (player.x == objectX-1 or player.x == objectX+1) and player.y == objectY then
		b = dialogEnter(dialog, num)
	elseif (player.y == objectY-1 or player.y == objectY+1) and player.x == objectX then
		b = dialogEnter(dialog, num)
	end
	return b
end

function tip(tipX, tipY, dialog, num)
	local b = false
	if player.x == tipX and player.y == tipY and player.floor == "*" then
		forcedDialog = true
		b = dialogEnter(dialog, num)
	end
	return b
end

function introTip(dialog, num)
	local b = false
	forcedDialog = true
	b = dialogEnter(dialog, num)
	return b
end

function inventoryCHECK(name)
	local i
	local back = false
	for i = 1, #inventory.all do
		if inventory.all[i][1] == name then
			if inventory.all[i][3] == true then
				back = true
			end
		end
	end
	return back
end

function inventoryIN(name)
	local i
	for i = 1, #inventory.all do
		if inventory.all[i][1] == name then
			inventory.all[i][3] = true
		end
	end
end

function inventoryOUT(name)
	local i
	for i = 1, #inventory.all do
		if inventory.all[i][1] == name then
			inventory.all[i][3] = false
		end
	end
end
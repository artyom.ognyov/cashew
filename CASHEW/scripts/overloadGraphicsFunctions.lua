-- OVERLOAD GRAPHICS FUNCTIONS

graphics = {}

function graphics.print(text, x, y)
	love.graphics.print(text, x + dw, y + dh)
end

function graphics.draw(drawable, x, y)
	love.graphics.draw(drawable, x + dw, y + dh)
end

function graphics.rectangle(mode, x, y, w, h)
	love.graphics.rectangle(mode, x + dw, y + dh, w, h)
end

function graphics.triangle(mode, x1, y1, x2, y2, x3, y3)
	love.graphics.polygon(mode, x1 + dw, y1 + dh, x2 + dw, y2 + dh, x3 + dw, y3 + dh)
end
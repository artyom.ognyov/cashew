-- FUNCTIONS FOR ENEMIES

function enemyMovement(x,time)

		--[[
		if enemiesTick[x] == true then

			enemiesTick[x] = false

			local prevX = 0
			local prevY = 0

			for i = 1, mapSize do
				for j = 1, mapSize do

						if map[j][i] == x and j~=prevY and i~=prevX then
							prevX = i
							prevY = j
							if math.abs(i - player.x) > 4 and math.abs(j - player.y) > 4 then
								local direction = love.math.random(1, 4)
								if direction == 1 and map[j-1][i] == " " then
									map[j-1][i] = map[j][i]
									map[j][i] = " "
									prevY = j-1
								elseif direction == 2 and map[j+1][i] == " " then
									map[j+1][i] = map[j][i]
									map[j][i] = " "
									prevY = j+1
								elseif direction == 3 and map[j][i+1] == " " then
									map[j][i+1] = map[j][i]
									map[j][i] = " "
									prevX = i+1
								elseif direction == 4 and map[j][i-1] == " " then
									map[j][i-1] = map[j][i]
									map[j][i] = " "
									prevX = i-1
								end
							else
								if player.x > i then
									if player.y > j then
										--up-right
										if map[j][i+1] == " " and map[j+1][i] == " " and map[j+1][i+1] == " " then
											map[j+1][i+1] = map[j][i]
											map[j][i] = " "
											prevX = i+1
											prevY = j+1
										elseif map[j][i+1] == " " and map[j+1][i] == " " then
											local direction = love.math.random(1,2)
											if direction == 1 then
												map[j][i+1] = map[j][i]
												map[j][i] = " "
												prevX = i+1
											else
												map[j+1][i] = map[j][i]
												map[j][i] = " "
												prevY = j+1
											end
										elseif map[j][i+1] == " " then
											map[j][i+1] = map[j][i]
											map[j][i] = " "
											prevX = i+1
										elseif map[j+1][i] == " " then
											map[j+1][i] = map[j][i]
											map[j][i] = " "
											prevY = j+1
										end

									elseif player.y < j then

										--down-right
										if map[j][i+1] == " " and map[j-1][i] == " " and map[j-1][i+1] == " " then
											map[j-1][i+1] = map[j][i]
											map[j][i] = " "
											prevX = i+1
											prevY = j-1
										elseif map[j][i+1] == " " and map[j-1][i] == " " then
											local direction = love.math.random(1,2)
											if direction == 1 then
												map[j][i+1] = map[j][i]
												map[j][i] = " "
												prevX = i+1
											else
												map[j-1][i] = map[j][i]
												map[j][i] = " "
												prevY = j-1
											end
										elseif map[j][i+1] == " " then
											map[j][i+1] = map[j][i]
											map[j][i] = " "
											prevX = i+1
										elseif map[j-1][i] == " " then
											map[j-1][i] = map[j][i]
											map[j][i] = " "
											prevY = j-1
										end

									else -- equal y
										
										--right
										if map[j][i+1] == " " then
											map[j][i+1] = map[j][i]
											map[j][i] = " "
											prevX = i+1
										end

									end
								elseif player.x < i then
									if player.y > j then

										--up-left
										if map[j][i-1] == " " and map[j+1][i] == " " and map[j+1][i-1] == " " then
											map[j+1][i-1] = map[j][i]
											map[j][i] = " "
											prevX = i-1
											prevY = j+1
										elseif map[j][i-1] == " " and map[j+1][i] == " " then
											local direction = love.math.random(1,2)
											if direction == 1 then
												map[j][i-1] = map[j][i]
												map[j][i] = " "
												prevX = i-1
											else
												map[j+1][i] = map[j][i]
												map[j][i] = " "
												prevY = j+1
											end
										elseif map[j][i-1] == " " then
											map[j][i-1] = map[j][i]
											map[j][i] = " "
											prevX = i-1
										elseif map[j+1][i] == " " then
											map[j+1][i] = map[j][i]
											map[j][i] = " "
											prevY = j+1
										end

									elseif player.y < j then

										--down-left
										if map[j][i-1] == " " and map[j-1][i] == " " and map[j-1][i-1] == " " then
											map[j-1][i-1] = map[j][i]
											map[j][i] = " "
											prevX = i-1
											prevY = j-1
										elseif map[j][i-1] == " " and map[j-1][i] == " " then
											local direction = love.math.random(1,2)
											if direction == 1 then
												map[j][i-1] = map[j][i]
												map[j][i] = " "
												prevX = i-1
											else
												map[j-1][i] = map[j][i]
												map[j][i] = " "
												prevY = j-1
											end
										elseif map[j][i-1] == " " then
											map[j][i-1] = map[j][i]
											map[j][i] = " "
											prevX = i-1
										elseif map[j-1][i] == " " then
											map[j-1][i] = map[j][i]
											map[j][i] = " "
											prevY = j-1
										end

									else -- equal y

										--left
										if map[j][i-1] == " " then
											map[j][i-1] = map[j][i]
											map[j][i] = " "
											prevX = i-1
										end

									end
								else -- equal x
									if player.y > j then

										-- down
										if map[j+1][i] == " " then
											map[j+1][i] = map[j][i]
											map[j][i] = " "
											prevY = j+1
										end

									elseif player.y < j then

										-- up
										if map[j-1][i] == " " then
											map[j-1][i] = map[j][i]
											map[j][i] = " "
											prevY = j-1
										end

									end
								end
							end

						end
				end
			end


			Timer.after(time,function()
				enemiesTick[x] = true
			end)

		end
		--]]

end




function enemyFighting()
	--[[
	if jumpState == 1 then
		if love.keyboard.isDown("c") or love.keyboard.isDown("l") then
			local ceil = math.ceil(player.weaponDistance/2)
			for i = 1, player.weaponDistance do
				for j = 1, player.weaponDistance do
					if map[player.y-ceil+j][player.x-ceil+i]~="@" and checkEnemy(map[player.y-ceil+j][player.x-ceil+i]) then
						play(correct)
						map[player.y-ceil+j][player.x-ceil+i] = ","
					end
				end
			end
		end
	end
	--]]
end
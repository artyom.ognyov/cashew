-- LEVEL LOGIC CHAPTER 1

function levelLogicChapter1()

	--Rails
	if mapN == 1 then

		--[[
		if global05 == true then
			global05 = false
			Timer.after(0.5, function()
				global05 = true
			end)
		end

		if global1 == true then
			global1 = false

			Timer.after(1, function()
				global1 = true
			end)
		end
		--]]

		object(29, 24, table, 1)
		object(2, 24, table, 2)
		object(19, 21, table, 3)
		object(3, 21, table, 4)
		object(17, 29, stranger, 1)
		object(26, 22, stranger, 2)
		jump(16, 20, chapter1.map1, chapter1.map2, 2, 11, 20)

	end

	--Railway Station
	if mapN == 2 then

		object(6,14,table,5)
		object(10,15,stranger,3)
		
		if defaults.chapter1.inventory1 == false then
			if defaults.chapter1.talkedWithUsher == false then
				defaults.chapter1.talkedWithUsher = object(16,14,usher,1)
			else
				if language == "en" then
					hud.pop = "press Z"
				elseif language == "ru" then
					hud.pop = "нажми Z"
				end
				object(16,14,usher,2)
			end
		else
			if inventory.all.chapter1[2][3] then
				object(16,14,usher,4)
			else
				if object(16,14,usher,3) then
					play(wrong)
					inventory.all.chapter1[2][3] = true
				end
			end
		end

		if inventory.all.chapter1[2][3] then
			if object(13,11,conductor,2) then
				if getCell(11,10,'x') then
					--play(wrong)
					setCell(11,10,' ')
				end
			end
		else
			object(13,11,conductor,1)
		end

		jump(11, 21, chapter1.map2, chapter1.map1, 1, 16, 21)
		jump(11, 9, chapter1.map2, chapter1.map3, 3, 13, 27)

	end

	--Station Backyard
	if mapN == 3 then

		object(9, 28, table, 6)
		object(15, 17, table, 7)
		tip(13, 20, nuss, 1)
		jump(17, 2, chapter1.map3, chapter1.map4, 4, 3, 40)
		jump(18, 2, chapter1.map3, chapter1.map4, 4, 4, 40)
	
	end

	--Forest Path (Don't go down)
	if mapN == 4 then
		
		object(21, 22, table, 8)
		jump(3, 41, chapter1.map4, chapter1.map3, 3, 17, 3)
		jump(4, 41, chapter1.map4, chapter1.map3, 3, 18, 3)
		jump(48, 34, chapter1.map4, chapter1.map5, 5, 4, 5)
		jump(48, 35, chapter1.map4, chapter1.map5, 5, 4, 6)
		jump(48, 47, chapter1.map4, chapter1.map5, 5, 4, 18)
		jump(48, 48, chapter1.map4, chapter1.map5, 5, 4, 19)
	
	end

	--Forest Path (Cemetery)
	if mapN == 5 then
		
		object(17, 26, table, 9)
		jump(3, 5, chapter1.map5, chapter1.map4, 4, 47, 34)
		jump(3, 6, chapter1.map5, chapter1.map4, 4, 47, 35)
		jump(3, 18, chapter1.map5, chapter1.map4, 4, 47, 47)
		jump(3, 19, chapter1.map5, chapter1.map4, 4, 47, 48)
		jump(56, 57, chapter1.map5, chapter1.map6, 6, 5, 36)
		jump(56, 58, chapter1.map5, chapter1.map6, 6, 5, 37)
	
	end

	--Forest Path (Gates)
	if mapN == 6 then

		object(16, 9, table, 10)
		object(22, 9, table, 10)
		jump(4, 36, chapter1.map6, chapter1.map5, 5, 55, 57)
		jump(4, 37, chapter1.map6, chapter1.map5, 5, 55, 58)
		jump(13, 2, chapter1.map6, chapter1.map7, 7, 13, 38)
		jump(14, 2, chapter1.map6, chapter1.map7, 7, 14, 38)
		jump(15, 2, chapter1.map6, chapter1.map7, 7, 15, 38)
		jump(16, 2, chapter1.map6, chapter1.map7, 7, 16, 38)
		jump(17, 2, chapter1.map6, chapter1.map7, 7, 17, 38)
		jump(18, 2, chapter1.map6, chapter1.map7, 7, 18, 38)
		jump(19, 2, chapter1.map6, chapter1.map7, 7, 19, 38)
		jump(20, 2, chapter1.map6, chapter1.map7, 7, 20, 38)
		jump(21, 2, chapter1.map6, chapter1.map7, 7, 21, 38)
		jump(22, 2, chapter1.map6, chapter1.map7, 7, 22, 38)
		jump(23, 2, chapter1.map6, chapter1.map7, 7, 23, 38)
		jump(24, 2, chapter1.map6, chapter1.map7, 7, 24, 38)
		jump(25, 2, chapter1.map6, chapter1.map7, 7, 25, 38)
		jump(39, 21, chapter1.map6, chapter1.map9, 9, 3, 18)
		jump(39, 22, chapter1.map6, chapter1.map9, 9, 3, 19)
		jump(39, 29, chapter1.map6, chapter1.map9, 9, 3, 26)
		jump(39, 30, chapter1.map6, chapter1.map9, 9, 3, 27)

	end

	--Front Yard
	if mapN == 7 then

		object(19, 34, table, 11)
		object(8, 22, table, 12)
		object(30, 22, table, 13)
		object(8, 11, table, 14)
		object(30, 11, table, 15)
		jump(13, 39, chapter1.map7, chapter1.map6, 6, 13, 3)
		jump(14, 39, chapter1.map7, chapter1.map6, 6, 14, 3)
		jump(15, 39, chapter1.map7, chapter1.map6, 6, 15, 3)
		jump(16, 39, chapter1.map7, chapter1.map6, 6, 16, 3)
		jump(17, 39, chapter1.map7, chapter1.map6, 6, 17, 3)
		jump(18, 39, chapter1.map7, chapter1.map6, 6, 18, 3)
		jump(19, 39, chapter1.map7, chapter1.map6, 6, 19, 3)
		jump(20, 39, chapter1.map7, chapter1.map6, 6, 20, 3)
		jump(21, 39, chapter1.map7, chapter1.map6, 6, 21, 3)
		jump(22, 39, chapter1.map7, chapter1.map6, 6, 22, 3)
		jump(23, 39, chapter1.map7, chapter1.map6, 6, 23, 3)
		jump(24, 39, chapter1.map7, chapter1.map6, 6, 24, 3)
		jump(25, 39, chapter1.map7, chapter1.map6, 6, 25, 3)
		jump(2, 20, chapter1.map7, chapter1.map8, 8, 97, 6)
		jump(2, 21, chapter1.map7, chapter1.map8, 8, 97, 7)
		jump(39, 20, chapter1.map7, chapter1.map9, 9, 3, 3)
		jump(39, 21, chapter1.map7, chapter1.map9, 9, 3, 4)

	end

	--Labyrinth
	if mapN == 8 then

		jump(98, 6, chapter1.map8, chapter1.map7, 7, 3, 20)
		jump(98, 7, chapter1.map8, chapter1.map7, 7, 3, 21)

	end

	--Cliff
	if mapN == 9 then

		jump(2, 3, chapter1.map9, chapter1.map7, 7, 38, 20)
		jump(2, 4, chapter1.map9, chapter1.map7, 7, 38, 21)
		jump(2, 18, chapter1.map9, chapter1.map6, 6, 38, 21)
		jump(2, 19, chapter1.map9, chapter1.map6, 6, 38, 22)
		jump(2, 26, chapter1.map9, chapter1.map6, 6, 38, 29)
		jump(2, 27, chapter1.map9, chapter1.map6, 6, 38, 30)

	end




-- end of fucnction
end
-- DIALOGS FOR THE GAME
-- GLOBAL

look = {} -- don't remove
look.en = {
	{ {"The door is closed.",""} }
}
look.en.name = ""
look.ru = {
	{ {"Дверь закрыта.",""} }
}
look.ru.name = ""

intro = {}
intro.en = {
	--chapter1
	{
		{"Hello","my friend"},
		{"That's me","again"}
	}
}
intro.en.name = "NUSS"
intro.ru = {
	--chapter1
	{
		{"Привет", "мой друг."},
		{"Это снова я.",""},
		{"Ты готов начать","своё путешестиве?"},
		{"...",""},
		{"Тогда приступим","..."}
	}
}
intro.ru.name = "НУСС"
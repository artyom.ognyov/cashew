-- INVENTORY FOR GAME

inventory.all = {}

inventory.all.chapter1 = {

		{
			{"PASSPORT",
				{
					"[ @ ]",
					"NAME: Nuss",
					"BD: 1985",
					"MS: widower",
					"DEPARTURE:",
					"allowed",
					""
				}
			},
			{"ПАСПОРТ",
				{
					"[ @ ]",
					"ИМЯ: Нусс",
					"ДР: 1985",
					"СП: вдовец",
					"ВЫЕЗД:",
					"разрешён",
					""
				}
			},
			true
		},

		{
			{"PASS",
				{
					"This pass",
					"allows you",
					"to locate",
					"in the city",
					"Geist.",
					"14 days",
					""
				}
			},
			{"ПРОПУСК",
				{
					"Этот пропуск",
					"разрешает вам",
					"находится на",
					"территории",
					"города Гейст.",
					"14 дней",
					""
				}
			},
			false
		}

	}
--[[

 ▄████████    ▄████████    ▄████████    ▄█    █▄       ▄████████  ▄█     █▄  
███    ███   ███    ███   ███    ███   ███    ███     ███    ███ ███     ███ 
███    █▀    ███    ███   ███    █▀    ███    ███     ███    █▀  ███     ███ 
███          ███    ███   ███         ▄███▄▄▄▄███▄▄  ▄███▄▄▄     ███     ███ 
███        ▀███████████ ▀███████████ ▀▀███▀▀▀▀███▀  ▀▀███▀▀▀     ███     ███ 
███    █▄    ███    ███          ███   ███    ███     ███    █▄  ███     ███ 
███    ███   ███    ███    ▄█    ███   ███    ███     ███    ███ ███ ▄█▄ ███ 
████████▀    ███    █▀   ▄████████▀    ███    █▀      ██████████  ▀███▀███▀  

	This is game code for "CASHEW"
	v 0.1.3
	(c) 2019
	by Pokato Poco Show

--]]

require ("utf8")
require ("scripts/engine")
moonshine = require 'moonshine'

effect = moonshine(moonshine.effects.chromasep)
	.chain(moonshine.effects.scanlines)
	.chain(moonshine.effects.crt)
effect.chromasep.angle = 1
effect.chromasep.radius = 1.5
effect.scanlines.opacity = 0.25
effect.scanlines.width = 2
effect.crt.feather = 0.2
effect.crt.distortionFactor = {1.025, 1.025}

effect.changeSize = false

effectIsEnabled = true

if effectIsEnabled == false then
	effect.disable("chromasep", "scanlines", "crt")
end

-- DEBUG MODE
fastMute =  false
fastLevel = 0
fastX = 12
fastY = 24

-- LOAD
function love.load()
	
	engineLoad()

	-- contents
	require ("contents/globalDialog")
	require ("contents/chapter1Dialog")
	require ("contents/chapter2Dialog")
	require ("contents/chapter3Dialog")
	require ("contents/chapter4Dialog")
	require ("contents/chapter5Dialog")
	require ("contents/chapter6Dialog")
	require ("contents/inventory")
	require ("contents/about")

	--levels
	require ("levelLogic/levelLogicChapter1")
end

-- UPDATE
function love.update(dt)

	--if OS == 'Windows' then
		-- [[ FPS CUT
		if dt < 1/60 then
			love.timer.sleep(1/60 - dt)	
		end
		--]]
	--end

	engineUpdate()
	-- GAME LOGIC
	if currentChapter == 1 then
		levelLogicChapter1()
	end

	-- Update timer
	Timer.update(dt)
end

-- MOVEMENT  
engineMovement()
engineKeyboard()

-- DRAW
function love.draw()
	effect(function()	
		if effect.changeSize == true then
			effect.changeSize = false
			effect.resize(ww, wh)
		end
		engineDraw()
		--love.graphics.setColor(white)
		--graphics.print(tostring(keyIsReadyToBePressed), -ww/2, -wh/2)
	end)
end
